import App from '../app';

import isPingable from '../util/is-pingable'

import isOwnerAtHome from '../util/is-owner-at-home';

import Logger from '../util/logger'

export default class ToggleCleaningWhileNotAtHome {
    private interval!:NodeJS.Timer;
    private disconnectedTicks:number = 0;
    private lastCleanTimestamp:number = 0;
    private logger:Logger = new Logger( 'cleaning-while-not-at-home' );
    private isCleaningViaScript:Boolean = false;
    private readonly app:App;

    constructor( app:App ) {
        this.app = app;

        this.interval = setInterval( () => {
            this.cycle();
        }, 1000 * 30 );

        this.cycle();
    }

    private cycle():void {
        isOwnerAtHome( this.app ).then( async ( _isOwnerAtHome:Boolean ) => {

            if( !_isOwnerAtHome ) {
                this.disconnectedTicks++;
            } else {
                this.disconnectedTicks = 0;
                this.logger.log( 'Looks like a master is at home' );
            }

            if( this.disconnectedTicks > 1 && this.app.vacuumCleaner ) {  
                
                // make sure there is internet connection
                if( ! ( await isPingable( '8.8.8.8' ) ) ) return;
                
                if( !this.app.vacuumCleaner.isCleaning() && ( Date.now() - this.lastCleanTimestamp > 3600 * 2000 ) ) {
                    this.logger.log( "Looks like a master is not at home, so let's do some cleaning" );

                    this.app.vacuumCleaner.requestCleaning();

                    this.isCleaningViaScript = true;
                } 
            } 

            // go to dock if owner is back
            if( _isOwnerAtHome && this.isCleaningViaScript ) {
                this.logger.log( 'Owner is back, stopping the task.' );
                if( this.app.vacuumCleaner?.isCleaning() ) {
                    const requestResult = await this.app.vacuumCleaner.requestGoToDock();
                    if( requestResult ) {
                        this.isCleaningViaScript = false;
                    }
                } else {
                    this.isCleaningViaScript = false;
                }
            }

        }).catch( ( e:any ) => {
            this.logger.log( e );
        });
    }
}