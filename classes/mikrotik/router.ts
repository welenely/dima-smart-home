const MikroNode = require( 'mikrotik' );

import { DHCPClient } from './DHCPClient'
import { WirelessClient } from './WirelessClient';

export class Router {
    private ip:string;
    protected connection:any;
    protected device:any = null;
    private user:string;
    private password:string;
    
    constructor( ip:string, user:string, password:string ) {
        this.ip = ip;
        this.user = user;
        this.password = password;
    }
    connect():Promise<Boolean> {
        return new Promise( ( resolve, reject ) => {
            try {
                if ( this.connection ) {
                    resolve( true );
                }
                let connectionPtr:any = MikroNode.getConnection( this.ip, this.user, this.password );
                connectionPtr.connect( ( connection: any ) => {
                    this.connection = connection;
                    resolve( true );
                } );
            }catch(e:any) {
                reject(e);
            }
        }) 
    }
    private uniqueid():string {
        return Date.now().toString(36) + Math.random().toString(36).substr(2);
    }
    protected rawExecuteAPIRequest( path:string, value:string = '' ):Promise<Array<String>> {
        return new Promise( ( resolve, reject ) => {
            if( !this.connection ) {
                reject( 'no_connection' );
            }
            let channel:any = this.connection.openChannel( this.uniqueid() ); // open a named channel
    
            channel.write( path, value );
    
            channel.on('done', ( data:any ) => {
            
                channel.close(); // close the channel. It is not autoclosed by default.
                
                resolve( data );
            });            
        } ) 
    }
    public listDHCPClients():Promise<Array<DHCPClient>> {
        return new Promise( ( resolve, reject ) => {
            this.rawExecuteAPIRequest( '/ip/dhcp-server/lease/getall' ).then( ( list:Array<String> ) => {
                resolve( DHCPClient.parseClients( list ) );
            } ).catch( e => reject( e ) ) ;
        } );
    }

    public listWirelessClients():Promise<Array<WirelessClient>> {
        return new Promise( ( resolve, reject ) => {
            this.rawExecuteAPIRequest( '/interface/wireless/registration-table/getall' ).then( ( list:Array<String> ) => {
                resolve( WirelessClient.parseClients( list ) );
            } ).catch( e => reject( e ) ) ;
        } );
    }
}
