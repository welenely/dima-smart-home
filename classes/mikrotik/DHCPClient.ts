const MikroNode = require( 'mikrotik' );

export class DHCPClient {
    public readonly address:string;
    public readonly MAC:string;
    public readonly clientId:string;
    public readonly server:string;
    public readonly activeAddress:string;
    public readonly activeMACAddress:string;
    public readonly activeHostName:string;
    constructor( ...args:Array<string> ) {
        this.address = args[0];
        this.MAC = args[1];
        this.clientId = args[2];
        this.server = args[3];
        this.activeAddress = args[4];
        this.activeMACAddress = args[5];
        this.activeHostName = args[6];
    }
    
    static parseClients( rawInput:any ) : Array<DHCPClient> {
        let rawArray:Array<object> = MikroNode.parseItems( rawInput );
        let fieldNeedle:Array<string> = [ 'address', 'mac-address', 'client-id', 'server', 'active-address', 'active-mac-address', 'host-name' ];
        let output:Array<DHCPClient> = [];
        rawArray.forEach( ( item:any ) => {
            let fields:Array<string> = [];
            for( let i : number = 0; i < fieldNeedle.length; i++ ) {
                if( item.hasOwnProperty( fieldNeedle[i] ) ) {
                    fields.push( item[fieldNeedle[i]] )
                }
            }
            output.push( new DHCPClient( ...fields ) );
        } );
        return output;
    }
}
