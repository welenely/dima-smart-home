const MikroNode = require( 'mikrotik' );

export class WirelessClient {
    public readonly MAC:string;
    constructor( ...args:Array<string> ) {
        this.MAC = args[0];
    }
    
    static parseClients( rawInput:any ) : Array<WirelessClient> {
        let rawArray:Array<object> = MikroNode.parseItems( rawInput );
        let fieldNeedle:Array<string> = [ 'mac-address' ];
        let output:Array<WirelessClient> = [];
        rawArray.forEach( ( item:any ) => {
            let fields:Array<string> = [];
            for( let i : number = 0; i < fieldNeedle.length; i++ ) {
                if( item.hasOwnProperty( fieldNeedle[i] ) ) {
                    fields.push( item[fieldNeedle[i]] )
                }
            }
            output.push( new WirelessClient( ...fields ) );
        } );
        return output;
    }
}
