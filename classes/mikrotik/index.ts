import { DHCPClient } from './DHCPClient' 
import { Router } from './router'
import { WirelessClient } from './WirelessClient'

export {
    Router,
    DHCPClient,
    WirelessClient
}

export default {
    Router,
    DHCPClient,
    WirelessClient
}