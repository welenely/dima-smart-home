const moment = require( 'moment' );
import path from 'path';
import fs from 'fs';

export default class Logger {
    private context:string = '';
    constructor( context:string = '' ) {
        this.context = context;
    }

    private getDateTimeStr():string {
        return `${moment(Date.now()).format("DD/MM/yy, hh:mm:ss")}`;
    }

    public log( message:string) {
        console.log( `[${this.context}] [${this.getDateTimeStr()}] ${message}` );

        const basePath = path.resolve( process.cwd() );
        const logsPath = path.join( basePath, '/logs' );

        if( !fs.existsSync( logsPath ) ) {
            fs.mkdirSync( logsPath );
        }

        fs.writeFileSync( path.join( logsPath, `${this.context}.txt` ), `[${this.getDateTimeStr()}] ${message}\n`, { flag: 'a+' } );
    }
}