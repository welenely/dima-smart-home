const ping = require('ping');

export default function isPingable( host:string ):Promise<Boolean> {
    return new Promise( ( resolve ) => {
        ping.promise.probe( host ).then( (response:any) => {
            resolve( response?.alive) ;
        } );
    });
}