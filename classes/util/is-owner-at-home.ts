import App from '../app';
import { WirelessClient as MikrotikWirelessClient } from '../mikrotik';

export default function isOwnerAtHome( app:App ):Promise<Boolean> {
    return new Promise( ( resolve, reject ) => {
        app.router.listWirelessClients().then( async ( clients:Array<MikrotikWirelessClient> ) => {
            clients.forEach( ( client: MikrotikWirelessClient ) => {
                if( app.config.owner.mac.includes( client.MAC ) ) { 
                    resolve( true );
                    return;
                }
            } );
            resolve( false );
        } ).catch( (e:any) => {
            reject( e );
        });
    } );
}