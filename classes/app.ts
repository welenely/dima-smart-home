import path from 'path';
import VacuumCleaner from './vacuum-cleaner';
import Logger from './util/logger';
import { Router as MikrotikRouter, DHCPClient as MikrotikDHCPClient } from './mikrotik';

export default class App {
    public readonly config:any;
    public readonly basePath:string;
    private _vacuumCleaner!:VacuumCleaner;
    private _logger!:Logger;
    private _router!:MikrotikRouter;

    constructor( ) {
        this.basePath = path.resolve( process.cwd() );
        this.config = require( path.join( this.basePath, './config.local' ) );
    }

    public init():Promise<void> {
        return new Promise( ( resolve, reject ) => {
            this._logger = new Logger( 'common' );

            this._router = new MikrotikRouter( this.config.mikrotik.ip, this.config.mikrotik.user, this.config.mikrotik.password );

            this._router.connect().then( () => {
                return this._router.listDHCPClients();
            } )
            .then( async (clients:Array<MikrotikDHCPClient>) => {
                clients.forEach( ( client: MikrotikDHCPClient ) => {
                    if( client.MAC === this.config.xiaomimop.MAC ) { 
                        this._vacuumCleaner = new VacuumCleaner( this, client.activeAddress );
                        this._vacuumCleaner.init();
                        resolve();
                        return;
                    }
                } );
                if( !this._vacuumCleaner ) {
                    throw 'No Xiaomi mop mac address found on the DHCP client list';
                } 
            } ).catch( ( e:any ) => {
                this._logger.log( e.toString() );
                reject();
            });
        })
    }

    public get logger() {
        return this._logger;
    }

    public get vacuumCleaner() {
        return this._vacuumCleaner;
    }

    public get router() {
        return this._router;
    }
}
