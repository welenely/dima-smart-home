const mihome = require('@dmpr-dev/node-mihome');

mihome.miioProtocol.init();

import App from '../app'

export default class VacuumCleaner {
    private cleanerInterface:any;
    public readonly mopIP:string = '';
    private readonly app:App;

    constructor( app: App, mopIP:string ) {
        this.mopIP = mopIP;
        this.app = app;
        this.cleanerInterface = mihome.device({
            id: this.app.config.xiaomimop.deviceId, // required, device id
            model: this.app.config.xiaomimop.deviceModel, // required, device model
          
            address: this.mopIP, // miio-device option, local ip address
            token: this.app.config.xiaomimop.token, // miio-device option, device token
            refresh: 20000 // miio-device option, device properties refresh interval in ms
        });
    }

    public init():void {
        this.cleanerInterface.init();
    }

    // getters

    public isCleaning():Boolean {
        return this.cleanerInterface.getState() === 'cleaning';
    }

    // setters

    private async _requestRobotCMD( command:string, arg:string = '' ):Promise<Boolean> {
        const robotResponse:Array<string> = await this.cleanerInterface[command](arg);

        if( robotResponse?.[0] != 'ok' ) {
            this.app.logger.log( `Unable to execute command - ${command}: ${robotResponse}` );
            return false;
        } else {
            return true;
        }
    }

    public async requestCleaning():Promise<Boolean> {
        return this._requestRobotCMD( 'setClean' );
    }

    public async requestGoToDock():Promise<Boolean> {
        return this._requestRobotCMD( 'setCharge', '1' );
    }
}