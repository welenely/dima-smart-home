import App from './classes/app';
import scenarios from './classes/scenarios'

let app:App = new App();

app.init().then( () => {
    scenarios.forEach( Scenario => {
        new Scenario( app );
    });
}).catch();